//
//  satScoreDetails.swift
//  20220708-PramelaMal-NYCSchools
//
//  Created by Pramela on 7/8/22.
import Foundation

struct NYCSchoolsSatScore : Codable {
    
    let dbn : String
    let schoolName : String
    let readingScore : String
    let mathScore : String
    let writingScore : String
    
    enum CodingKeys : String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
}
