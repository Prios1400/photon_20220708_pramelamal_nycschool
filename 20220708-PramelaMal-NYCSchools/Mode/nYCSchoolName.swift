//
//  nYCSchoolName.swift
//  20220708-PramelaMal-NYCSchools
//
//  Created by Pramela on 7/8/22.
//

import Foundation

struct SchoolDetails : Codable {
    
    let dbn : String
    let school_name : String
    
    enum Codingkeys : String, CodingKey {
        case dbn
        case school_name
    }
}
