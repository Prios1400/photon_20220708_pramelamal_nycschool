//
//  nYCSchoolNamesViewController.swift
//  20220708-PramelaMal-NYCSchools
//
//  Created by Pramela on 7/8/22.
//

import UIKit

class nYCSchoolNamesViewController: UIViewController {
    
    var nYCSchoolarray = [SchoolDetails]()
    
    @IBOutlet weak var tableView: UITableView!
    
    ///
    ///This method is to call the get school details /Users/pramela/Desktop/Xcodeplayground/20220708-PramelaMal-NYCSchools/20220708-PramelaMal-NYCSchoolsAPI to get the school name and display the school names in the table view
    ///
    func getSchoolNames () {
        
        if let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"){
            URLSession.shared.dataTask(with: url){(data,response,error) in
                if let data = data {
                    let jsonDecoder = JSONDecoder()
                    do{
                        let parseJson = try jsonDecoder.decode([SchoolDetails].self, from: data)
                        DispatchQueue.main.async {
                            self.nYCSchoolarray = parseJson
                            self.tableView.reloadData()
                        }
                    }
                    catch{
                        print("error:\(error) ")
                    }
                }
            }.resume()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getSchoolNames()
        tableView.delegate = self
        tableView.dataSource = self
    }
}

//passing the school names to the table view cell
extension nYCSchoolNamesViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nYCSchoolarray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolNameCell", for: indexPath)
        cell.textLabel?.text = nYCSchoolarray[indexPath.row].school_name
        
        return cell
        
    }
    
    // calling satscore view controller and passing the Dbn and School Name
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let getSatScore  : satScoreViewController = self.storyboard?.instantiateViewController(withIdentifier: "satScoreVC") as! satScoreViewController
        getSatScore.dbnText = nYCSchoolarray[indexPath.row].dbn ?? ""
        getSatScore.schoolText = nYCSchoolarray[indexPath.row].school_name
        self.navigationController?.pushViewController(getSatScore, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
