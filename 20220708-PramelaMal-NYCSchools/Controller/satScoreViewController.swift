//
//  satScoreViewController.swift
//  20220708-PramelaMal-NYCSchools
//
//  Created by Pramela on 7/8/22.
//

import UIKit

class satScoreViewController: UIViewController {
    
    var nYCSchoolSatScore = [NYCSchoolsSatScore]()
    
    @IBOutlet weak var dbnLabel: UILabel!
    @IBOutlet weak var schoolLabel: UILabel!
    @IBOutlet weak var writeLabel: UILabel!
    @IBOutlet weak var mathLabel: UILabel!
    @IBOutlet weak var readingLabel: UILabel!
    var schoolText : String = ""
    var dbnText : String = ""
    
    ///
    ///This method is to get the Sat Scores from the API and Display it in Sat Scores view controller
    ///
    func getSatScoreOfNYCSchools() -> [NYCSchoolsSatScore]{
        
        var satResults  = [NYCSchoolsSatScore]()
        if let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json") {
            URLSession.shared.dataTask(with: url){data,response,error in
                if let data = data {
                    let jsonDecoder = JSONDecoder()
                    do{
                        //decode the response
                        let parsedJson = try  jsonDecoder.decode([NYCSchoolsSatScore].self, from: data)
                       // filtering the data using Dbn , found that school names are not always a match, Also its always good to query on unique ids
                        satResults = parsedJson.filter{$0.dbn == self.dbnText}
                        if satResults.count > 0
                        {
                        DispatchQueue.main.async {
                                                      self.schoolLabel.text = satResults[0].schoolName
                                                      self.readingLabel.text = satResults[0].readingScore
                                                      self.mathLabel.text = satResults[0].mathScore
                                                      self.writeLabel.text = satResults[0].writingScore
                      
                                                  }
                        }
                        else
                        {
                            // Gracefully handling the cases where there is no Sat scores available , Displaying the school name since we have it available in the schools api.
                            DispatchQueue.main.async {
                            self.schoolLabel.text = self.schoolText
                            self.readingLabel.text = "Not available"
                            self.mathLabel.text = "Not available"
                            self.writeLabel.text = "Not available"
                            }
                        }
                         
                    }
                    catch {
                        // catching any errors from json decoding .. i always use this as structures are tricky :)
                        print("error : \(error)")
                    }
                }
                
                
            }.resume()
        }
        return satResults
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getSatScoreOfNYCSchools()
        
    }
}
